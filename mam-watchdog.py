import sys,json,time,os,requests,pickle
from datetime import datetime
from enum import Enum

class LogLevel(Enum):
    NONE=0
    ERROR=1
    WARN=2
    INFO=3
    DEBUG=4

    def __lt__(self, other):
        if self.__class__ is other.__class__:
            return self.value < other.value
        return NotImplemented

r_session = requests.Session()

def error(msg, force = False):
    if force or log_level > LogLevel.NONE: print(f"[ERROR] {msg}")

def warn(msg, force = False):
    if force or log_level > LogLevel.ERROR: print(f"[WARN] {msg}")

def info(msg, force = False):
    if force or log_level > LogLevel.WARN: print(f"[INFO] {msg}")

def debug(msg, force = False):
    if force or log_level > LogLevel.INFO: print(f"[DEBUG] {msg}")

log_level_env = "NONE" if os.getenv('LOG_LEVEL') is None else str(os.getenv('LOG_LEVEL'))
if(log_level_env):
    i = log_level_env.lower()
    if(i == 'none'):
        log_level = LogLevel.NONE
    elif (i == 'error'):
        log_level = LogLevel.ERROR
    elif (i == 'warn'):
        log_level = LogLevel.WARN
    elif (i == 'info'):
        log_level = LogLevel.INFO
    elif (i == 'debug'):
        log_level = LogLevel.DEBUG
else:
    log_level = LogLevel.INFO

debug(f"log_level: {log_level}", True) # always output log level
debug(f"log_level_env: {log_level_env}")

url_dynip = "https://t.myanonamouse.net/json/dynamicSeedbox.php"
debug(f"url_mam: {url_dynip}")
url_points = "https://www.myanonamouse.net/json/bonusBuy.php/?spendtype=upload&amount=Max%20Affordable%20"
# url_points = "https://www.myanonamouse.net/json/userBonusHistory.php"
debug(f"url_points: {url_points}")
id_mam = os.getenv('MAM_ID')
debug(f"id_mam: {id_mam}")

dir_data = "/mam/" if os.getenv('MAM_DIR') is None else str(os.getenv('MAM_DIR'))
debug(f"dir_data: {dir_data}")
dir_data = os.path.join(dir_data,"") # make sure data_dir has a trailing /
debug(f"dir_data: {dir_data}")

file_cookies = "cookies" if os.getenv('FILE_COOKIES') is None else str(os.getenv('FILE_COOKIES'))
debug(f"file_cookies: {file_cookies}")
file_cookies_stale_after = (6 * 60 * 60) if os.getenv('FILE_COOKIES_STALE') is None else int(os.getenv('FILE_COOKIES_STALE')) # 6 hours
debug(f"file_cookies_stale_after: {file_cookies_stale_after}")
file_ip = "ip" if os.getenv('FILE_IP') is None else str(os.getenv('FILE_IP'))
debug(f"file_ip: {file_ip}")

time_sleep = 600 if os.getenv('SLEEP') is None else int(os.getenv('SLEEP'))
debug(f"time_sleep: {time_sleep}")
time_sleep_error = 60 if os.getenv('SLEEP_ERROR') is None else int(os.getenv('SLEEP_ERROR'))
debug(f"time_sleep_error: {time_sleep_error}")

allowed_buying = False if os.getenv('ALLOWED_BUYING') is None else (True if os.getenv('ALLOWED_BUYING').lower() in ['yes','true','on'] else False)
debug(f"allowed_buying: {allowed_buying}")
interval_buying = (60 * 60) if os.getenv('INTERVAL_BUYING') is None else int(os.getenv('INTERVAL_BUYING')) # 1h
debug(f"interval_buying: {interval_buying}")
file_log_buying = "time_bought_upload" if os.getenv('FILE_LOG_BUYING') is None else str(os.getenv('FILE_LOG_BUYING'))
debug(f"file_log_buying: {file_log_buying}")

while True:
    is_ip_changed = False
    is_cookies_stale = False
    is_error = False

    # Check if cookies have been updated > file_cookies_stale_after
    mtime_cookies = os.path.getmtime(f"{dir_data}{file_cookies}")
    time_now = (datetime.now() - datetime(1970, 1, 1)).total_seconds()
    debug(f"time_now: {str(time_now)}")
    if mtime_cookies + file_cookies_stale_after < time_now:
        warn("Cookies stale")
        debug(f"cookies_mtime = {mtime_cookies}, now_time: {time_now}")
        is_cookies_stale = True

    # Check if stored IP is the same as current IP
    ip_current = requests.get("https://ipinfo.io/json", verify = True).json()['ip']

    try:
        with open(f"{dir_data}{file_ip}", 'r') as f:
            ip_old = f.read()
            debug(f"current_ip: {ip_current}, old_ip: {ip_old}")
            if ip_current != ip_old:
                warn("IP Changed")
                is_ip_changed = True
    except FileNotFoundError:
        warn("IP file doesn't exist")

    # Try to get mam_id from the cookie file
    try:
        with open(f"{dir_data}{file_cookies}", 'rb') as f:
            r_session.cookies.update(pickle.load(f))
    except FileNotFoundError:
        warn("Cookies file doesn't exit")

    # Try to get mam_id from the environment variable MAM_ID
    if r_session.cookies.get('mam_id') == None:
        r_session.cookies.set('mam_id', id_mam , domain=".myanonamouse.net")

    # Exit if couldn't get mam_id
    if r_session.cookies.get('mam_id') == None:
        warn("No mam_id available")
        is_error = True

    if not is_error and (is_cookies_stale or is_ip_changed):
        debug(f"Cookies content before: {r_session.cookies.get_dict()}")

        # Call the API
        r_ip = r_session.get(url_dynip)
        r_ip_success = r_ip.json()['Success']
        r_ip_msg = r_ip.json()['msg']

        debug(f"Response: {r_ip.text}")
        debug(f"Cookies content after: {r_session.cookies.get_dict()}")
        # If successful write the current IP and session cookies to appropriate files
        # Otherwise retry in 30 seconds
        if r_ip_success:
            info(f"Successfully updated IP: {r_ip_msg}")

            with open(f"{dir_data}{file_ip}","w") as f:
                f.write(ip_current)

            with open(f"{dir_data}{file_cookies}", 'wb') as f:
                pickle.dump(r_session.cookies, f)

        else:
            error(f"Couldn't update IP: {r_ip_msg}")
            is_error = True
    else:
        if not is_error:
            debug("No need to update IP")

    s = time_sleep

    if not is_error:
        if allowed_buying:
            buying_points = True
            try:
                with open(f"{dir_data}{file_log_buying}", 'r') as f:
                    last_bought = float(f.read() or -1)
                    debug(f"last_bought: {last_bought}")
                    if last_bought + interval_buying > time_now:
                        buying_points = False
            except FileNotFoundError:
                warn("Last time points bought file doesn't exist")

            if buying_points:
                r_points = r_session.get(url_points)
                debug(r_points.text)
                try:
                    r_points_success = r_points.json()['success']
                    if r_points_success:
                        info(f"Bought {r_points.json()['amount']}GB of upload")
                        with open(f"{dir_data}{file_log_buying}","w") as f:
                            f.write(str(time_now))
                    else:
                        try:
                            error(f"Error buying upload: {r_points.json()['error']}")
                        except:
                            error("Error buying upload")
                except KeyError as e:
                    error(f"Cannot find {e} in response: {r_points.text}")
            else:
                debug("Not buying points")
        else:
            debug("Not allowed to buy points")
    else:
        s = time_sleep_error

    debug(f"Sleeping for {s} seconds")
    time.sleep(s)
