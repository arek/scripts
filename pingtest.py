import argparse
import sys
from ping3 import ping, verbose_ping


def fastest_ip(domains, ping_times):
        domain_times = []

        for domain in domains:
                print(f"Pinging {domain}...")
                times = [ping(domain) for _ in range(ping_times)]
                avg_time = sum([t for t in times if t is not None]) / len(times)
                domain_times.append((domain, avg_time))

        domain_times.sort(key=lambda x: x[1])

        if len(domain_times) > 10:
                domain_times = domain_times[:10]

        return domain_times


def main():
        parser = argparse.ArgumentParser()
        parser.add_argument("domains", help="Comma-separated list of IPs or domains to ping", type=str)
        parser.add_argument("-p", "--ping_count", help="Specify the number of times to ping. Default is 5", type=int,
                                                default=5)

        args = parser.parse_args()
        domains = args.domains.split(',')

        fastest_domains = fastest_ip(domains, args.ping_count)

        for domain, time in fastest_domains:
                print(f"{domain} : {time}")


if __name__ == "__main__":
        main()


