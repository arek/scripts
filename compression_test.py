#!/usr/bin/python3

import argparse
import glob
import os
import shutil
import subprocess
import sys
import time
import uuid

commands_dict = {
    "tar": [
        "tar -cvf {0}.tar {1}",
        "tar -czvf {0}.tar.gz {1}",
        "tar -cjvf {0}.tar.bz2 {1}",
    ],
    "7z": [
        "7z a {0}.7z {1}", # default
        "7z a -t7z -m0=lzma2 -mx=9 -mfb=64 -md=32m -ms=on {0}.7z {1}", # "ultra"
        "7z a -t7z -m0=lzma2 -mx=9 -mfb=273 -md=512m -ms=on {0}.7z {1}",
    ],
    "zip": ["zip {0}.zip {1}", "zip -9 {0}.zip {1}"],
    "gzip": ["gzip -c {1} > {0}.gz", "gzip -9 -c {1} > {0}.gz"],
    "bzip2": ["bzip2 -c {1} > {0}.bz2", "bzip2 -9 -c {1} > {0}.bz2"],
    "xz": ["xz -z -c {1} > {0}.xz", "xz -9 -z -c {1} > {0}.xz"],
    "lzma": ["lzma -z -c {1} > {0}.lzma", "lzma -9 -z -c {1} > {0}.lzma"],
    "lzip": ["lzip -c {1} > {0}.lz", "lzip -9 -c {1} > {0}.lz"],
    "lrzip": ["lrzip -o {0}.lrz {1}", "lrzip -z -o {0}.lrz {1}"],
    "zstd": ["zstd -c {1} > {0}.zst", "zstd -19 -c {1} > {0}.zst"],
    "rar": ["rar a {0}.rar {1}", "rar a -m5 -s {0}.rar {1}"],
    "brotli": ["brotli -c {1} > {0}.br", "brotli -9 -c {1} > {0}.br"],
    "lzma2": [
        "xz --format=lzma2 --compress --stdout {1} > {0}.xz",
        "xz --format=lzma2 -9 --compress --stdout {1} > {0}.xz",
    ],
    "lha": ["lha a {0}.lzh {1}"],
}

parser = argparse.ArgumentParser(description="Process a file.")
parser.add_argument(
    "Path", metavar="path", type=str, help="The path to the file to process"
)
args = parser.parse_args()
path_to_file = args.Path
initial_size = os.path.getsize(path_to_file)


def compress_and_measure(program, command):
    output_file_base = "/tmp/" + str(uuid.uuid4())
    full_command = command.format(output_file_base, path_to_file)

    start_time = time.time()

    print(f"{program} => {command}: ", end="")
    sys.stdout.flush()

    subprocess.run(
        full_command, shell=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL
    )

    end_time = time.time()
    elapsed_time = end_time - start_time

    compressed_files = glob.glob(f"{output_file_base}.*")

    for compressed_file in compressed_files:
        final_size = os.path.getsize(compressed_file)
        ratio = final_size / initial_size
        print(f"{ratio * 100:.2f}% in {elapsed_time:.2f}s")
        os.remove(compressed_file)


for program, commands in commands_dict.items():
    if shutil.which(program) is not None:
        for command in commands:
            compress_and_measure(program, command)
    else:
        print(f"{program} is not installed. Skipping...")
