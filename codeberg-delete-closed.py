import requests

codeberg_token = "4a2fd34caf6cabc41969b2261f0a4aaed1e4b599"

repo = "https://codeberg.org/api/v1/repos/arek.codes/nuMatrix"

headers = {
    'accept': 'application/json',
}

while True:
    closed_issues = requests.get(f"{repo}/issues", params={
        'state': 'closed',
        'limit': '100',
        'access_token': codeberg_token,
    }, headers=headers).json()

    if len(closed_issues) == 0:
        quit()

    for issue in closed_issues:
        print(f"{repo}/issues/{issue['number']}")
        del_response = requests.delete(f"{repo}/issues/{issue['number']}", params={
            'access_token': codeberg_token,
        }, headers=headers)
        print(del_response.status_code)

