function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}
_btns = document.querySelectorAll('[aria-label^="Click to stop following"]');
for (i = 0; i < _btns.length; ++i) {
    _btns[i].click()
    await sleep(100)
    document.getElementsByClassName('artdeco-modal__confirm-dialog-btn')[1].click()
}
