import argparse
import re
from rand_utils import RandomUtils


def get_dice_rolls_required(word_dict):
    lengths = {len(k) for k in word_dict.keys()}
    if len(lengths) > 1:
        raise ValueError("All words in the list must require the same number of dice rolls.")
    return lengths.pop()


def create_dictionary_from_file(file_path):
    word_dict = {}
    with open(file_path, 'r') as file:
        for line in file:
            match = re.match(r'^(\d+)\s+(\w+)', line.strip())
            if match:
                word_dict[match.group(1)] = match.group(2)
    return word_dict


def generate_word(word_dict, dice_rolls_required, entropy, util):
    while True:
        random_number = ''.join(str(util.rand_num(entropy, 1, 6)) for _ in range(dice_rolls_required))
        print('.', end='', flush=True)
        if random_number in word_dict:
            return word_dict[random_number]


def main():
    parser = argparse.ArgumentParser(description='Diceware password generator.')
    parser.add_argument('-l', '--list', type=str, default="lists/original",
                        help='The directory of a word list file (default=lists/original)')

    parser.add_argument('-w', '--words', type=int, default=6,
                        help='How many words to generate (default=6).')

    parser.add_argument('-c', '--count', type=int, default=1,
                        help='How many passwords to generate (default=1).')

    parser.add_argument('-e', '--entropy', type=str, default="1KB",
                        help='How much entropy to gather for each dice roll specified in bytes or with units GB, MB, KB. (e.g. "1024", "100KB", "5MB", "2GB", default=1kb).')

    args = parser.parse_args()

    word_dict = create_dictionary_from_file(args.list)

    print(f"Found {len(word_dict)} words")

    util = RandomUtils()

    for i in range(args.count):
        dice_rolls_required = get_dice_rolls_required(word_dict)
        generated_words = [generate_word(word_dict, dice_rolls_required, args.entropy, util) for _ in range(args.words)]

        print()
        print(' '.join(generated_words))


if __name__ == '__main__':
    main()
