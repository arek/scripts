
class RandomUtils:
    def _size_to_bytes(self, size):
        units = {"kb": 1024, "mb": 1024 ** 2, "gb": 1024 ** 3}
        if size[-2:].lower() in units:
            return int(size[:-2]) * units[size[-2:].lower()]
        elif size[-1:].lower() in units:
            return int(size[:-1]) * units[size[-1:].lower()]
        else:
            return int(size)

    def _prase_entropy(self, entropy):
        try:
            return self._size_to_bytes(entropy)
        except ValueError:
            raise Exception("Invalid entropy size provided")

    def rand_num(self, entropy="1kb", lower_bound=1, upper_bound=6):
        entropy_bytes = self._prase_entropy(entropy)
        with open('/dev/random', 'rb') as f:
            random_data = f.read(entropy_bytes)
        range_size = upper_bound - lower_bound + 1
        random_number = lower_bound + int.from_bytes(random_data, byteorder='big') % range_size
        return random_number

    def rand_char(self, entropy="1kb", ascii=False, alphanum=False, unicode=False):
        if ascii:
            return chr(self.rand_num(entropy=entropy, lower_bound=0, upper_bound=127))  # ASCII range
        elif alphanum:
            return chr(
                self.rand_num(entropy=entropy, lower_bound=48, upper_bound=122))  # Alphanumeric range (digits, upper and lower case letters)
        elif unicode:
            return chr(
                self.rand_num(entropy=entropy, lower_bound=0, upper_bound=0x10FFFF))  # Full Unicode range according to the Unicode Standard
        else:
            return None
