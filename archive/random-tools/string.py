import argparse
from rand_utils import RandomUtils


def generate_string(rand_util, str_len, entropy, ascii, alphanum, unicode, live_print):
    if live_print:
        for _ in range(str_len):
            next_char = rand_util.rand_char(ascii=ascii, alphanum=alphanum, unicode=unicode, entropy=entropy)
            print(next_char, end='', flush=True)
        print()  # line break after each string
    else:
        return ''.join(rand_util.rand_char(ascii=ascii, alphanum=alphanum, unicode=unicode, entropy=entropy) for _ in
                       range(str_len))


def main():
    parser = argparse.ArgumentParser(description='Random string generator.')

    parser.add_argument('-s', '--strings', type=int, default=1,
                        help='How many strings to generate (default=1).')

    parser.add_argument('-l', '--length', type=int, default=10,
                        help='Length of each string to generate (default=10).')

    parser.add_argument('-a', '--ascii', action='store_true',
                        help='Generate ASCII characters.')

    parser.add_argument('-n', '--alphanum', action='store_true',
                        help='Generate alphanumeric characters.')

    parser.add_argument('-u', '--unicode', action='store_true',
                        help='Generate Unicode characters.')

    parser.add_argument('-e', '--entropy', type=str, default="1KB",
                        help='How much entropy to gather for each character specified in bytes or with units GB, MB, KB. (e.g. "1024", "100KB", "5MB", "2GB", default=1kb).')

    parser.add_argument('-lp', '--live_print', action='store_true',
                        help='Print each character live as it is generated.')

    args = parser.parse_args()

    if not args.unicode and not args.ascii and not args.alphanum:
        args.alphanum = True

    rand_util = RandomUtils()

    for _ in range(args.strings):
        generated_string = generate_string(rand_util, args.length, args.entropy, args.ascii, args.alphanum,
                                           args.unicode, args.live_print)
        if not args.live_print:
            print(generated_string)

if __name__ == '__main__':
    main()
