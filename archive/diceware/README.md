Script to generate diceware passwords using /dev/random as an entropy source. 

Depends on /dev/random providing real random numbers (see [rng-tools](https://github.com/nhorman/rng-tools))

### Usage (diceware.py -h)
```
diceware.py [-h] [-l LIST] [-w WORDS] [-c COUNT] [-e ENTROPY]

options:
  -h, --help            show this help message and exit
  -l LIST, --list LIST  The directory of a word list file (default=lists/original)
  -w WORDS, --words WORDS
                        How many words to generate (default=6).
  -c COUNT, --count COUNT
                        How many passwords to generate (default=1).
  -e ENTROPY, --entropy ENTROPY
                        How much entropy to gather for each dice roll specified in bytes or with units GB, MB, KB. (e.g. "1024", "100KB", "5MB", "2GB", default=1kb).
```
Sources for lists:

- original - https://web.archive.org/web/20240223163408/https://theworld.com/~reinhold/diceware.wordlist.asc

- eff - https://www.eff.org/deeplinks/2016/07/new-wordlists-random-passphrases

- pl - https://web.archive.org/web/20080926125905/http://www.drfugazi.eu.org/diceware

- pl-short - https://github.com/MaciekTalaska/diceware-pl