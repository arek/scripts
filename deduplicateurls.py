from urllib.parse import urlparse


def get_domains_from_file(input_file, output_file):
    with open(input_file, 'r') as in_file:
        urls = in_file.read().splitlines()

    domains = set()
    for url in urls:
        if '://' not in url:
            url = 'http://' + url
        parsed_url = urlparse(url)
        split_domain = parsed_url.netloc.split('.')
        domain = '.'.join(split_domain[-2:]) if len(split_domain) > 1 else split_domain[0]
        if domain:
            domains.add(domain)

    with open(output_file, 'w') as out_file:
        for domain in domains:
            out_file.write(domain + '\n')


get_domains_from_file('input.txt', 'output.txt')

