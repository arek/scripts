#!/bin/bash
proc=nextcloud
if tmux has-session -t $proc > /dev/null 2>&1; then
    :
else
    tmux new-session -d -s $proc "cd /opt/nextcloud; cd git; git pull; cd ..; docker compose build --pull; docker compose up"
fi

